=== Wallet One Payment ===
Contributors: Wallet One
Version: 3.0.0
Tags: Wallet One, Readyscript, buy now, payment, payment for Readyscript
Requires at least: 2.0.3.219
Tested up to: 2.0.5.239
Stable tag: 2.0.5.239
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One module is a payment system for Readyscript. He it allows to pay for your orders on the site.

== Description ==
If you have an online store on Readyscript, then you need a module payments for orders made. This will help you plug the payment system Wallet One. With our system you will be able to significantly expand the method of receiving payment. This will lead to an increase in the number of customers to your store.

The latest version of the module you can be found at the link https://bitbucket.org/h_elena/readyscript-w1/downloads

== Installation ==
1. Register on the site http://www.walletone.com
2. Download the module files on this and upload to the server in a folder /modules/.
3. Activate the module.
4. Instructions for configuring the module is in the file read.pdf or go to site https://www.walletone.com/ru/merchant/modules/readyscript/ and read instuction.

== Screenshots ==

== Changelog ==
= 1.0.0 =
*- Adding a module

= 1.0.1 =
*- Fixed anser from cms

= 1.0.2 =
*- Fixed bug for php 5.3, array assignment

= 1.0.3 =
*- Fixed bug for function ormbeforewriteshoppayment for static

= 2.0.0 =
*- Added new universal classes

= 2.0.1 =
*- Fidex bug with checked signature

= 3.0.0 =
* Fix - fixed bug with anser from calback payment system

== Frequently Asked Questions ==
No recent asked questions 

== Upgrade Notice ==
No recent asked updates

