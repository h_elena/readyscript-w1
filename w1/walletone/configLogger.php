<?php
$path = dirname(str_replace('\\', '/', __DIR__)).'/../';

return array(
  'rootLogger' => array(
    'appenders' => array('default'),
  ),
  'appenders' => array(
    'default' => array(
      'class' => 'LoggerAppenderFile',
      'layout' => array(
        'class' => 'LoggerLayoutPattern',
        'conversionPattern' => '%date [%logger] %message%newline'
      ),
      'params' => array(
        'file' => $path . 'log/w1PaymentSystem.log',
        'append' => true
      )
    )
  ),
);

