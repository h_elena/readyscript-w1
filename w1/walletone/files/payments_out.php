<?php

return array(
  0 => array(
    'id' => 'web',
    'name' => 'Электронные деньги',
    'data' => array(
      0 => array(
        'id' => 'WalletOne',
        'name' => 'Единый кошелек',
        'data' => array(
          0 => array(
            'id' => 'WalletOneRUB',
            'name' => 'W1 RUB',
            'required' => 1
          ),
          1 => array(
            'id' => 'WalletOneUAH',
            'name' => 'W1 UAH'
          ),
          2 => array(
            'id' => 'WalletOneUSD',
            'name' => 'W1 USD'
          ),
          3 => array(
            'id' => 'WalletOneEUR',
            'name' => 'W1 EUR'
          ),
          4 => array(
            'id' => 'WalletOneZAR',
            'name' => 'W1 ZAR'
          ),
          5 => array(
            'id' => 'WalletOneBYR',
            'name' => 'W1 BYR'
          ),
          6 => array(
            'id' => 'WalletOneGEL',
            'name' => 'W1 GEL'
          ),
          7 => array(
            'id' => 'WalletOneKZT',
            'name' => 'W1 KZT'
          ),
          8 => array(
            'id' => 'WalletOnePLN',
            'name' => 'W1 PLN'
          ),
          9 => array(
            'id' => 'WalletOneTJS',
            'name' => 'W1 TJS'
          ),
        ),
      ),
      1 => array(
        'id' => 'Yandex',
        'name' => 'Яндекс.Деньги',
        'data' => array(
          0 => array(
            'id' => 'YandexMoneyRUB',
            'name' => 'Яндекс.Деньги (RUB)',
            'required' => 1
          ),
        ),
      ),
      2 => array(
        'id' => 'UniMoney',
        'name' => 'Юнистрим Деньги',
        'data' => array(
          0 => array(
            'id' => 'UniMoneyRUB',
            'name' => 'Юнистрим Деньги (RUB)'
          ),
        ),
      ),
      3 => array(
        'id' => 'WebMoney',
        'name' => 'WebMoney',
        'data' => array(
          0 => array(
            'id' => 'WebMoneyRUB',
            'name' => 'WMR',
            'required' => 1
          ),
          1 => array(
            'id' => 'WebMoneyBYR',
            'name' => 'WMB'
          ),
        ),
      ),
      4 => array(
        'id' => 'QiwiWallet',
        'name' => 'QIWI Кошелек',
        'data' => array(
          0 => array(
            'id' => 'QiwiWalletRUB',
            'name' => 'QIWI Кошелек (RUB)',
            'required' => 1
          ),
        ),
      ),
      5 => array(
        'id' => 'BPay',
        'name' => 'B-pay',
        'data' => array(
          0 => array(
            'id' => 'BPayMDL',
            'name' => 'B-pay (MDL)'
          ),
        ),
      ),
      6 => array(
        'id' => 'CashU',
        'name' => 'CashU',
        'data' => array(
          0 => array(
            'id' => 'CashUUSD',
            'name' => 'CashU (USD)'
          ),
        ),
      ),
      7 => array(
        'id' => 'EasyPay',
        'name' => 'EasyPay',
        'data' => array(
          0 => array(
            'id' => 'EasyPayBYR',
            'name' => 'EasyPay (BYR)'
          ),
        ),
      ),
      8 => array(
        'id' => 'LiqPayMoney',
        'name' => 'LiqPay Money',
        'data' => array(
          0 => array(
            'id' => 'LiqPayMoneyRUB',
            'name' => 'LiqPayMoneyRUB'
          ),
          1 => array(
            'id' => 'LiqPayMoneyUAH',
            'name' => 'LiqPayMoneyUAH'
          ),
        ),
      ),
      9 => array(
        'id' => 'GoogleWallet',
        'name' => 'Google Wallet',
        'data' => array(
          0 => array(
            'id' => 'GoogleWalletUSD',
            'name' => 'Google Wallet (USD)'
          ),
        ),
      ),
      10 => array(
        'id' => 'OKPAY',
        'name' => 'OKPAY',
        'data' => array(
          0 => array(
            'id' => 'OkpayUSD',
            'name' => 'OKPAY USD'
          ),
          1 => array(
            'id' => 'OkpayRUB',
            'name' => 'OKPAY RUB'
          ),
        ),
      ),
    ),
  ),
  1 => array(
    'id' => 'nocash',
    'name' => 'Безналичные',
    'data' => array(
      0 => array(
        'id' => 'cart',
        'name' => 'Банковские карты',
        'data' => array(
          0 => array(
            'id' => 'CreditCardBYR',
            'name' => 'Банковские карты BYR',
            'required' => 1
          ),
          1 => array(
            'id' => 'CreditCardRUB',
            'name' => 'Банковские карты RUB'
          ),
          2 => array(
            'id' => 'CreditCardUAH',
            'name' => 'Банковские карты UAH'
          ),
          3 => array(
            'id' => 'CreditCardUSD',
            'name' => 'Банковские карты USD'
          ),
          4 => array(
            'id' => 'CreditCardEUR',
            'name' => 'Банковские карты EUR'
          ),
          5 => array(
            'id' => 'SmartiviGEL',
            'name' => 'Карты Smartivi GEL'
          ),
        ),
      ),
      1 => array(
        'id' => 'OnlineBank',
        'name' => 'Интернет-банки',
        'data' => array(
          0 => array(
            'id' => 'AlfaclickRUB',
            'name' => 'Интернет-банк Альфа-Клик (Альфа-Банк) (RUB)'
          ),
          1 => array(
            'id' => 'Privat24UAH',
            'name' => 'Интернет-банк Приват24'
          ),
          2 => array(
            'id' => 'PsbRetailRUB',
            'name' => 'Интернет-банк PSB-Retail (Промсвязьбанк) (RUB)'
          ),
          3 => array(
            'id' => 'SberOnlineRUB',
            'name' => 'Сбербанк ОнЛ@йн (RUB)'
          ),
          4 => array(
            'id' => 'FakturaruRUB',
            'name' => 'Faktura.ru (RUB)'
          ),
          5 => array(
            'id' => 'RsbRUB',
            'name' => 'Интернет-банк Банка Русский Стандарт (RUB)'
          ),
          6 => array(
            'id' => 'EripBYR',
            'name' => 'Единое Расчетное Информационное Пространство (ЕРИП) (BYR)'
          ),
          7 => array(
            'id' => 'SetcomSidZAR',
            'name' => 'SetcomSid ZAR (ЮАР)'
          ),
          8 => array(
            'id' => 'StandardBankEftZ',
            'name' => 'Standard Bank EFT (ЮАР) (ZAR)'
          ),
        ),
      ),
      2 => array(
        'id' => 'BankTransfer',
        'name' => 'Банковский перевод',
        'data' => array(
          0 => array(
            'id' => 'BankTransferCNY',
            'name' => 'Банковский перевод в китайских юанях'
          ),
          1 => array(
            'id' => 'BankTransferEUR',
            'name' => 'Банковский перевод в евро'
          ),
          2 => array(
            'id' => 'BankTransferGEL',
            'name' => 'Банковский перевод в лари'
          ),
          3 => array(
            'id' => 'BankTransferKZT',
            'name' => 'Банковский перевод в тенге'
          ),
          4 => array(
            'id' => 'BankTransferMDL',
            'name' => 'Банковский перевод в леях'
          ),
          5 => array(
            'id' => 'BankTransferPLN',
            'name' => 'Банковский перевод в польских злотах'
          ),
          6 => array(
            'id' => 'BankTransferRUB',
            'name' => 'Банковский перевод в рублях'
          ),
          7 => array(
            'id' => 'BankTransferUAH',
            'name' => 'Банковский перевод в гривнах'
          ),
          8 => array(
            'id' => 'BankTransferUSD',
            'name' => 'Банковский перевод в долларах'
          ),
        ),
      ),
    ),
  ),
  2 => array(
    'id' => 'mobil',
    'name' => 'Мобильная коммерция',
    'data' => array(
      0 => array(
        'id' => 'BeelineRUB',
        'name' => 'Мобильный платеж Билайн (Россия)',
        'required' => 1
      ),
      1 => array(
        'id' => 'MtsRUB',
        'name' => 'Мобильный платеж МТС (Россия)',
        'required' => 1
      ),
      2 => array(
        'id' => 'MegafonRUB',
        'name' => 'Мобильный платеж Мегафон (Россия)',
        'required' => 1
      ),
      3 => array(
        'id' => 'Tele2RUB',
        'name' => 'Мобильный платеж Tele2 (Россия)',
        'required' => 1
      ),
      4 => array(
        'id' => 'KievStarUAH',
        'name' => 'КиевСтар.Мобильные деньги (Украина)'
      ),
    ),
  ),
  3 => array(
    'id' => 'cash',
    'name' => 'Наличные',
    'data' => array(
      0 => array(
        'id' => 'CashTerminal',
        'name' => 'Платежные терминалы',
        'data' => array(
          0 => array(
            'id' => 'CashTerminalBYR',
            'name' => 'Платежные терминалы Беларуси'
          ),
          1 => array(
            'id' => 'CashTerminalGEL',
            'name' => 'Платежные терминалы Грузии'
          ),
          2 => array(
            'id' => 'CashTerminalKZT',
            'name' => 'Платежные терминалы Казахстана'
          ),
          3 => array(
            'id' => 'CashTerminalMDL',
            'name' => 'Платежные терминалы Молдовы'
          ),
          4 => array(
            'id' => 'CashTerminalRUB',
            'name' => 'Платежные терминалы России'
          ),
          5 => array(
            'id' => 'CashTerminalUAH',
            'name' => 'Платежные терминалы Украины'
          ),
          6 => array(
            'id' => 'CashTerminalTJS',
            'name' => 'Платежные терминалы Таджикистана'
          ),
          7 => array(
            'id' => 'CashTerminalZAR',
            'name' => 'Платежные терминалы ЮАР'
          ),
        ),
      ),
      1 => array(
        'id' => 'ATM',
        'name' => 'ATM',
        'data' => array(
          0 => array(
            'id' => 'AtmRUB',
            'name' => 'Прием наличных (RUB)'
          ),
          1 => array(
            'id' => 'AtmUAH',
            'name' => 'Прием наличных (UAH)'
          ),
          2 => array(
            'id' => 'StandardAtmZAR',
            'name' => 'Standard Bank (ZAR)'
          ),
        ),
      ),
      2 => array(
        'id' => 'MobileRetails',
        'name' => 'Салоны связи',
        'data' => array(
          0 => array(
            'id' => 'MobileRetailsRUB',
            'name' => 'Салоны связи (RUB)'
          ),
          1 => array(
            'id' => 'EurosetRUB',
            'name' => 'Салоны связи Евросеть (RUB)'
          ),
          2 => array(
            'id' => 'SvyaznoyRUB',
            'name' => 'Салоны связи Связной (RUB)'
          ),
          3 => array(
            'id' => 'DixisRUB',
            'name' => 'Салоны связи Диксис (RUB)'
          ),
          4 => array(
            'id' => 'CifrogradRUB',
            'name' => 'Салоны связи Цифроград (RUB)'
          ),
          5 => array(
            'id' => 'CellularWorldRUB',
            'name' => 'Салоны связи Сотовый мир (RUB)'
          ),
        ),
      ),
      3 => array(
        'id' => 'BankOffice',
        'name' => 'Отделения банков',
        'data' => array(
          0 => array(
            'id' => 'SberbankRUB',
            'name' => 'Отделения Сбербанка России (RUB)'
          ),
          1 => array(
            'id' => 'SberbankKZT',
            'name' => 'Отделения Сбербанка в Казахстане (KZT)'
          ),
          2 => array(
            'id' => 'PrivatbankUAH',
            'name' => 'Отделения Приватбанка в Украине (UAH)'
          ),
          3 => array(
            'id' => 'PravexBankUAH',
            'name' => 'Отделения Правэкс-Банка в Украине (UAH)'
          ),
          4 => array(
            'id' => 'UkrsibBankUAH',
            'name' => 'Отделения УкрСиббанка в Украине (UAH)'
          ),
          5 => array(
            'id' => 'KazkomBankKZT',
            'name' => 'Отделения Казкоммерцбанка Казахстане (KZT)'
          ),
          6 => array(
            'id' => 'LibertyBankGEL',
            'name' => 'Отделения Liberty Bank в Грузии (GEL)'
          ),
          7 => array(
            'id' => 'StandardBankZAR',
            'name' => 'Отделения Standard Bank в ЮАР (ZAR)'
          ),
          8 => array(
            'id' => 'RussianPostRUB',
            'name' => 'Отделения Почты России (RUB)'
          ),
          9 => array(
            'id' => 'TinkoffRUB',
            'name' => 'Tinkoff (RUB)'
          ),
        ),
      ),
      4 => array(
        'id' => 'MoneyTransfer',
        'name' => 'Денежные переводы',
        'data' => array(
          0 => array(
            'id' => 'LiderRUB',
            'name' => 'Денежные переводы ЛИДЕР (RUB)'
          ),
          1 => array(
            'id' => 'UnistreamRUB',
            'name' => 'Денежные переводы Unistream (RUB)'
          ),
          2 => array(
            'id' => 'UnistreamUSD',
            'name' => 'Денежные переводы Unistream (USD)'
          ),
          3 => array(
            'id' => 'AnelikRUB',
            'name' => 'Денежные переводы Anelik (RUB)'
          ),
        ),
      ),
    ),
  )
);
