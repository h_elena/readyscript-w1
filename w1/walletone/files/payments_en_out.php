<?php

return array(
  0 => array(
    'id' => 'web',
    'name' => 'Electronic money',
    'data' => array(
      0 => array(
        'id' => 'WalletOne',
        'name' => 'Wallet One',
        'data' => array(
          0 => array(
            'id' => 'WalletOneRUB',
            'name' => 'W1 RUB',
            'required' => 1
          ),
          1 => array(
            'id' => 'WalletOneUAH',
            'name' => 'W1 UAH'
          ),
          2 => array(
            'id' => 'WalletOneUSD',
            'name' => 'W1 USD'
          ),
          3 => array(
            'id' => 'WalletOneEUR',
            'name' => 'W1 EUR'
          ),
          4 => array(
            'id' => 'WalletOneZAR',
            'name' => 'W1 ZAR'
          ),
          5 => array(
            'id' => 'WalletOneBYR',
            'name' => 'W1 BYR'
          ),
          6 => array(
            'id' => 'WalletOneGEL',
            'name' => 'W1 GEL'
          ),
          7 => array(
            'id' => 'WalletOneKZT',
            'name' => 'W1 KZT'
          ),
          8 => array(
            'id' => 'WalletOnePLN',
            'name' => 'W1 PLN'
          ),
          9 => array(
            'id' => 'WalletOneTJS',
            'name' => 'W1 TJS'
          ),
        ),
      ),
      1 => array(
        'id' => 'Yandex',
        'name' => 'Yandex.Money',
        'data' => array(
          0 => array(
            'id' => 'YandexMoneyRUB',
            'name' => 'Yandex.Money (RUB)'
          ),
        ),
      ),
      2 => array(
        'id' => 'UniMoney',
        'name' => 'Unistream Money',
        'data' => array(
          0 => array(
            'id' => 'UniMoneyRUB',
            'name' => 'Unistream Money (RUB)'
          ),
        ),
      ),
      3 => array(
        'id' => 'WebMoney',
        'name' => 'WebMoney',
        'data' => array(
          0 => array(
            'id' => 'WebMoneyRUB',
            'name' => 'WMR'
          ),
          1 => array(
            'id' => 'WebMoneyBYR',
            'name' => 'WMB'
          ),
        ),
      ),
      4 => array(
        'id' => 'QiwiWallet',
        'name' => 'QIWI wallet',
        'data' => array(
          0 => array(
            'id' => 'QiwiWalletRUB',
            'name' => 'QIWI wallet (RUB)'
          ),
        ),
      ),
      5 => array(
        'id' => 'BPay',
        'name' => 'B-pay',
        'data' => array(
          0 => array(
            'id' => 'BPayMDL',
            'name' => 'B-pay (MDL)'
          ),
        ),
      ),
      6 => array(
        'id' => 'CashU',
        'name' => 'CashU',
        'data' => array(
          0 => array(
            'id' => 'CashUUSD',
            'name' => 'CashU (USD)'
          ),
        ),
      ),
      7 => array(
        'id' => 'EasyPay',
        'name' => 'EasyPay',
        'data' => array(
          0 => array(
            'id' => 'EasyPayBYR',
            'name' => 'EasyPay (BYR)'
          ),
        ),
      ),
      8 => array(
        'id' => 'LiqPayMoney',
        'name' => 'LiqPay Money',
        'data' => array(
          0 => array(
            'id' => 'LiqPayMoneyRUB',
            'name' => 'LiqPayMoneyRUB'
          ),
          1 => array(
            'id' => 'LiqPayMoneyUAH',
            'name' => 'LiqPayMoneyUAH'
          ),
        ),
      ),
      9 => array(
        'id' => 'GoogleWallet',
        'name' => 'Google Wallet',
        'data' => array(
          0 => array(
            'id' => 'GoogleWalletUSD',
            'name' => 'Google Wallet (USD)'
          ),
        ),
      ),
      10 => array(
        'id' => 'OKPAY',
        'name' => 'OKPAY',
        'data' => array(
          0 => array(
            'id' => 'OkpayUSD',
            'name' => 'OKPAY USD'
          ),
          1 => array(
            'id' => 'OkpayRUB',
            'name' => 'OKPAY RUB'
          ),
        ),
      ),
    ),
  ),
  1 => array(
    'id' => 'nocash',
    'name' => 'Noncash',
    'data' => array(
      0 => array(
        'id' => 'cart',
        'name' => 'Bank cards',
        'data' => array(
          0 => array(
            'id' => 'CreditCardBYR',
            'name' => 'Bank cards BYR'
          ),
          1 => array(
            'id' => 'CreditCardRUB',
            'name' => 'Bank cards RUB'
          ),
          2 => array(
            'id' => 'CreditCardUAH',
            'name' => 'Bank cards UAH'
          ),
          3 => array(
            'id' => 'CreditCardUSD',
            'name' => 'Bank cards USD'
          ),
          4 => array(
            'id' => 'CreditCardEUR',
            'name' => 'Bank cards EUR'
          ),
          5 => array(
            'id' => 'SmartiviGEL',
            'name' => 'Bank cards GEL'
          ),
        ),
      ),
      1 => array(
        'id' => 'OnlineBank',
        'name' => 'Internet banking',
        'data' => array(
          0 => array(
            'id' => 'AlfaclickRUB',
            'name' => 'Online bank Alpha click (Alfa Bank) (RUB)',
          ),
          1 => array(
            'id' => 'Privat24UAH',
            'name' => 'Online bank Privat24'
          ),
          2 => array(
            'id' => 'PsbRetailRUB',
            'name' => 'Online bank PSB-Retail (Promsvyazbank) (RUB)',
          ),
          3 => array(
            'id' => 'SberOnlineRUB',
            'name' => 'Sberbank online (RUB)',
          ),
          4 => array(
            'id' => 'FakturaruRUB',
            'name' => 'Faktura.ru (RUB)'
          ),
          5 => array(
            'id' => 'RsbRUB',
            'name' => 'Online bank Russian standard (RUB)'
          ),
          6 => array(
            'id' => 'EripBYR',
            'name' => 'One Calculated information space (ERIP) (BYR)'
          ),
          7 => array(
            'id' => 'SetcomSidZAR',
            'name' => 'SetcomSid ZAR (UAR)'
          ),
          8 => array(
            'id' => 'StandardBankEftZ',
            'name' => 'Standard Bank EFT (UAR) (ZAR)'
          ),
        ),
      ),
      2 => array(
        'id' => 'BankTransfer',
        'name' => 'Bank transfer',
        'data' => array(
          0 => array(
            'id' => 'BankTransferCNY',
            'name' => 'Bank transfer in Chinese Yuan'
          ),
          1 => array(
            'id' => 'BankTransferEUR',
            'name' => 'Bank transfer in euro'
          ),
          2 => array(
            'id' => 'BankTransferGEL',
            'name' => 'Bank transfer in GEL'
          ),
          3 => array(
            'id' => 'BankTransferKZT',
            'name' => 'Bank transfer in KZT'
          ),
          4 => array(
            'id' => 'BankTransferMDL',
            'name' => 'Bank transfer in lei'
          ),
          5 => array(
            'id' => 'BankTransferPLN',
            'name' => 'Bank transfer in PLN'
          ),
          6 => array(
            'id' => 'BankTransferRUB',
            'name' => 'Bank transfer in rubles'
          ),
          7 => array(
            'id' => 'BankTransferUAH',
            'name' => 'Bank transfer in UAH'
          ),
          8 => array(
            'id' => 'BankTransferUSD',
            'name' => 'Bank transfer in USD'
          ),
        ),
      ),
    ),
  ),
  2 => array(
    'id' => 'mobil',
    'name' => 'Mobile commerce',
    'data' => array(
      0 => array(
        'id' => 'BeelineRUB',
        'name' => 'Mobile payment Beeline (Russia)'
      ),
      1 => array(
        'id' => 'MtsRUB',
        'name' => 'Mobile payment Mts (Russia)'
      ),
      2 => array(
        'id' => 'MegafonRUB',
        'name' => 'Mobile payment Megafon (Russia)'
      ),
      3 => array(
        'id' => 'Tele2RUB',
        'name' => 'Mobile payment Tele2 (Russia)'
      ),
      4 => array(
        'id' => 'KievStarUAH',
        'name' => 'Kyivstar.Mobile money (Ukraine)'
      ),
    ),
  ),
  3 => array(
    'id' => 'cash',
    'name' => 'Cash',
    'data' => array(
      0 => array(
        'id' => 'CashTerminal',
        'name' => 'Payment terminals',
        'data' => array(
          0 => array(
            'id' => 'CashTerminalBYR',
            'name' => 'Payment terminals of Belarus'
          ),
          1 => array(
            'id' => 'CashTerminalGEL',
            'name' => 'Payment terminals of Georgia'
          ),
          2 => array(
            'id' => 'CashTerminalKZT',
            'name' => 'Payment terminals of Kazakhstan'
          ),
          3 => array(
            'id' => 'CashTerminalMDL',
            'name' => 'Payment terminals of Moldova'
          ),
          4 => array(
            'id' => 'CashTerminalRUB',
            'name' => 'Payment terminals of Russia'
          ),
          5 => array(
            'id' => 'CashTerminalUAH',
            'name' => 'Payment terminals of Ukraine'
          ),
          6 => array(
            'id' => 'CashTerminalTJS',
            'name' => 'Payment terminals of Tajikistan'
          ),
          7 => array(
            'id' => 'CashTerminalZAR',
            'name' => 'Payment terminals of South Africa'
          ),
        ),
      ),
      1 => array(
        'id' => 'ATM',
        'name' => 'ATM',
        'data' => array(
          0 => array(
            'id' => 'AtmRUB',
            'name' => 'Receiving cash (RUB)'
          ),
          1 => array(
            'id' => 'AtmUAH',
            'name' => 'Receiving cash (UAH)'
          ),
          2 => array(
            'id' => 'StandardAtmZAR',
            'name' => 'Standard Bank (ZAR)'
          ),
        ),
      ),
      2 => array(
        'id' => 'MobileRetails',
        'name' => 'Salons connection',
        'data' => array(
          0 => array(
            'id' => 'MobileRetailsRUB',
            'name' => 'Salons connection (RUB)'
          ),
          1 => array(
            'id' => 'EurosetRUB',
            'name' => 'Salons connection Euroset (RUB)'
          ),
          2 => array(
            'id' => 'SvyaznoyRUB',
            'name' => 'Salons connection Svyaznoi (RUB)'
          ),
          3 => array(
            'id' => 'DixisRUB',
            'name' => 'Salons connection Dixis (RUB)'
          ),
          4 => array(
            'id' => 'CifrogradRUB',
            'name' => 'Salons connection Dixis (RUB)'
          ),
          5 => array(
            'id' => 'CellularWorldRUB',
            'name' => 'Salons connection Sotovi mir (RUB)'
          ),
        ),
      ),
      3 => array(
        'id' => 'BankOffice',
        'name' => 'Banks offices',
        'data' => array(
          0 => array(
            'id' => 'SberbankRUB',
            'name' => 'Offices Sberbank in Russia (RUB)'
          ),
          1 => array(
            'id' => 'SberbankKZT',
            'name' => 'Offices Sberbank in Kazakhstan (KZT)'
          ),
          2 => array(
            'id' => 'PrivatbankUAH',
            'name' => 'Offices Privatbank in Ukraine (UAH)'
          ),
          3 => array(
            'id' => 'PravexBankUAH',
            'name' => 'Office Pravex-Bank in Ukraine (UAH)'
          ),
          4 => array(
            'id' => 'UkrsibBankUAH',
            'name' => 'Offices of UkrSibbank in Ukraine (UAH)'
          ),
          5 => array(
            'id' => 'KazkomBankKZT',
            'name' => 'Offices Kazkommertsbank in Kazakhstan (KZT)'
          ),
          6 => array(
            'id' => 'LibertyBankGEL',
            'name' => 'Offices Liberty Bank in Georgia (GEL)'
          ),
          7 => array(
            'id' => 'StandardBankZAR',
            'name' => 'Offices Standard Bank in South Africa (ZAR)'
          ),
          8 => array(
            'id' => 'RussianPostRUB',
            'name' => 'Offices of Russian Post (RUB)'
          ),
          9 => array(
            'id' => 'TinkoffRUB',
            'name' => 'Tinkoff (RUB)'
          ),
        ),
      ),
      4 => array(
        'id' => 'MoneyTransfer',
        'name' => 'Money transfers',
        'data' => array(
          0 => array(
            'id' => 'LiderRUB',
            'name' => 'Money transfers Lider (RUB)'
          ),
          1 => array(
            'id' => 'UnistreamRUB',
            'name' => 'Money transfers Unistream (RUB)'
          ),
          2 => array(
            'id' => 'UnistreamUSD',
            'name' => 'Money transfers Unistream (USD)'
          ),
          3 => array(
            'id' => 'AnelikRUB',
            'name' => 'Money transfers Anelik (RUB)'
          ),
        ),
      ),
    ),
  )
);
