<?php

/**
 * Wallet One (https://www.walletone.com)
 *
 * @copyright Copyright (c) Wallet One (https://www.walletone.com)
 * @License: GPLv3
 * @License URI: http://www.gnu.org/licenses/gpl-3.0.en.html
 */

namespace W1\Config;

use \RS\Router;
use \RS\Orm\Type as OrmType;
use \RS\Html\Table\Type as TableType;
use \RS\Html\Filter;

class Handlers extends \RS\Event\HandlerAbstract {

  function init() {
    $this
        ->bind('getroute')
        ->bind('payment.gettypes')
        ->bind('orm.beforewrite.shop-payment')
        ->bind('controller.beforeexec.shop-front-onlinepay');
  }

  /**
   * Добавляем новый вид оплаты - Wallet One
   * 
   * @param array $list - массив уже существующих типов оплаты
   * @return array
   */
  public static function paymentGetTypes($list) {
    $list[] = new \W1\Model\PaymentType\W1();
    return $list;
  }
  
  /**
   * Возвращает маршруты данного модуля
   */
  public static function getRoute(array $routes) {
    $routes[] = new \RS\Router\Route('w1-front-onlinepayment', array('/w1/waiting/', '/w1/'), null, t('Страница с результатом платежа'));

    return $routes;
  }

  public static function ormbeforewriteshoppayment($data){
    if($data['orm']->class == 'w1') {
      if(!empty($data['orm']->data['generate']) && $data['orm']->data['generate'] == 1) {
        $ptenabled = (!empty($data['orm']->data['PTENABLED']) ? $data['orm']->data['PTENABLED'] : array());
        $ptdisabled = (!empty($data['orm']->data['PTDISABLED']) ? $data['orm']->data['PTDISABLED'] : array());
        include_once dirname(__DIR__).'/walletone/Classes/W1Client.php';
        $client = \WalletOne\Classes\W1Client::init()->run('ru', 'readyScript');

        $logger = \Logger::getLogger(__CLASS__);
        if($img_src = $client->createNewIcon($ptenabled, $ptdisabled)) {
          if (strpos(html_entity_decode($data['orm']->description), '<img') !== false) {
            if (preg_match('/<img([^&]*)">/', html_entity_decode($data['orm']->description), $matches)) {
              if (preg_match('/"([^&]*)/', $matches[1], $matches)) {
                $data['orm']->description = str_replace($matches[1], '/modules/w1/walletone/img/'.$img_src, html_entity_decode($data['orm']->description));
              }
            }
          }
          else {
            $data['orm']->description .= ' <img src="' . '/modules/w1/walletone/img/'. $img_src . '">';
          }
        }
      }
      else {
        if (strpos(html_entity_decode($data['orm']->description), '<img') !== false) {
          if (preg_match('/<img([^&]*)>/', html_entity_decode($data['orm']->description), $matches) !== false) {
            $data['orm']->description = str_replace($matches[0], '', html_entity_decode($data['orm']->description));
          }
        }
      }
    }
  }
  
  /*
   * Interception html form before request in remote server
   */
  public function controllerbeforeexecshopfrontonlinepay($data) {
    if ($data['action'] == 'doPay') {
      $request = $data['controller']->url;
      $data['controller']->wrapOutput(false);
      $order_id = $data['controller']->url->request('order_id', TYPE_STRING);
      $transactionApi = new \Shop\Model\TransactionApi();
      $transaction = $transactionApi->createTransactionFromOrder($order_id);
      if ($transaction->getPayment()->getTypeObject()->isPostQuery()) {
        $payment = $transaction->getPayment()->getTypeObject();
        if ($payment->getShortName() == 'w1') {
          $url = $transaction->getPayUrl();
          $data['controller']->wrapOutput(false);
          $view = new \RS\View\Engine();
          $view->assign(array(
            'url' => $url,
            'transaction' => $transaction
          ));
          $output = $view->fetch("%w1%/form/payment/w1/post.tpl");
          return array(
            'output' => $output
          );
        }
      }
    }
  }

}
