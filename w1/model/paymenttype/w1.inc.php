<?php

/**
 * Wallet One (https://www.walletone.com)
 *
 * @copyright Copyright (c) Wallet One (https://www.walletone.com)
 * @License: GPLv3
 * @License URI: http://www.gnu.org/licenses/gpl-3.0.en.html
 */

namespace W1\Model\PaymentType;

use \RS\Orm\Type;
use \Shop\Model\Orm\Transaction;

/**
 * Способ оплаты - Wallet One
 */
class W1 extends \Shop\Model\PaymentType\AbstractType {
  /**
   * Declaration of class client
   * 
   * @var object
   */
  private $client;
  
  private $logger;
      
  function __construct() {
    include_once dirname(__DIR__).'/../walletone/Classes/W1Client.php';
    $this->client = \WalletOne\Classes\W1Client::init()->run('ru', 'readyScript');
    
    $this->logger = \Logger::getLogger(__CLASS__);
  }

  /**
   * Возвращает название расчетного модуля (типа доставки)
   * 
   * @return string
   */
  function getTitle() {
    return w1Title;
  }

  /**
   * Возвращает описание типа оплаты. Возможен HTML
   * 
   * @return string
   */
  function getDescription() {
    return w1Desc;
  }

  /**
   * Возвращает идентификатор данного типа оплаты. (только англ. буквы)
   * 
   * @return string
   */
  function getShortName() {
    return 'w1';
  }

  /**
   * Отправка данных с помощью POST?
   * 
   */
  function isPostQuery() {
    return true;
  }

  /**
   * Возвращает ORM объект для генерации формы или null
   * 
   * @return \RS\Orm\FormObject | null
   */
  function getFormObject() {
    $payment_enabl = $this->client->getHtmlPayments('PTENABLED', $this->getOption('PTENABLED'), 'data');
    $payment_disabl = $this->client->getHtmlPayments('PTDISABLED', $this->getOption('PTDISABLED'), 'data');
    $properties = new \RS\Orm\PropertyIterator(array(
      'MERCHANT_ID' => new Type\Varchar(array(
        'description' => w1SettingsMerchantDesc,
        'hint' => w1SettingsMerchant,
      )),
      'signatureMethod' => new Type\Enum(array('default', 'throughout', 'forced'), array(
        'description' => w1SettingsSignatureMethod,
        'hint' => w1SettingsSignatureMethodDesc,
        'ListFromArray' => array($this->client->getSettings()->signatureMethodArray),
      )),
      'SIGNATURE' => new Type\Varchar(array(
        'description' => w1SettingsSignature,
        'hint' => w1SettingsSignatureDesc,
      )),
      'generate' => new Type\Integer(array(
        'maxLength' => 1,
        'description' => w1SettingsCreateIcon,
        'checkboxview' => array(1, 0),
      )),
      'CURRENCY_ID' => new Type\Enum(array('default', 'throughout', 'forced'), array(
        'allowEmpty' => false,
        'default' => w1SettingsCurrency_643,
        'description' => w1SettingsCurrency,
        'ListFromArray' => array($this->client->getSettings()->currencyName),
      )),
      'currency_default' => new Type\Integer(array(
        'maxLength' => 1,
        'description' => w1SettingsCurrencyDefault,
        'checkboxview' => array(1, 0),
        'hint' => w1SettingsCurrencyDefaultDesc,
      )),
      'PTENABLED' => new Type\ArrayList(array(
        'description' =>w1SettingsPtenabled,
        'template' => '%w1%/form/payment/list_payments_enabled.tpl',
        'payment' => $payment_enabl,
        'path' => '%w1%/../.././walletone/css/readyscript.css',
        'CheckboxListView' => true
      )),
      'PTDISABLED' => new Type\ArrayList(array(
        'description' => w1SettingsPtdisabled,
        'template' => '%w1%/form/payment/list_payments_disabled.tpl',
        'payment' => $payment_disabl,
        'CheckboxListView' => true
      )),
      'help' => new Type\Mixed(array(
        'description' => t(''),
        'visible' => true,
        'template' => '%w1%/form/payment/help.tpl',
        'text' => w1TextAboutIntegration
      ))
    ));

    return new \RS\Orm\FormObject($properties);
  }

  /**
   * Возвращает true, если данный тип поддерживает проведение платежа через интернет
   * 
   * @return bool
   */
  function canOnlinePay() {
    return true;
  }

  /**
   * Возвращает URL для перехода на сайт сервиса оплаты
   * 
   * @param Transaction $transaction
   * @return string
   */
  function getPayUrl(Transaction $transaction) {
    $params = $this->getOption();
    $order  = $transaction->getOrder();
    $router = \RS\Router\Manager::obj();
    $settings = array(
      'merchantId' => $params['MERCHANT_ID'],
      'signatureMethod' => $params['signatureMethod'],
      'signature' => $params['SIGNATURE'],
      'currencyId' => $params['CURRENCY_ID'],
      'currencyDefault' => $params['currency_default'],
      'orderStatusSuccess' => '',
      'orderStatusWaiting' => '',
      'cultureId' => 'ru-RU',
      'order_currency' => $order->currency,
      'summ' => number_format($transaction->cost, 2, '.', ''),
      'orderId' =>  $order['order_num'],
      'siteName' => \RS\Site\Manager::getSite()->full_title,
      'nameCms' => '_prestashop',
      'transaction' => $transaction->id
    );
    
    $url = $router->getUrl('shop-front-onlinepay', array('Act'=>'success', 'PaymentType'=>$this->getShortName()), true);
    $url .= (strpos($url, '?') == false ? '?' : '&').'id_order='.$order['order_num'];
    $settings['successUrl'] = $url;
    $settings['failUrl'] = $url;
    if(!empty($params['PTENABLED'])){
      $settings['paymentSystemEnabled'] = $params['PTENABLED'];
    }
    if(!empty($params['PTDISABLED'])){
      $settings['paymentSystemDisabled'] = $params['PTDISABLED'];
    }
    $user = $transaction->getUser();
    if(!empty($user->e_mail)) {
      $settings['emailBuyer'] = $user->e_mail;
    }
    if(!empty($user->name)) {
      $settings['firstNameBuyer'] = $user->name;
    }
    if(!empty($user->surname)) {
      $settings['lastNameBuyer'] = $user->surname;
    }
    
    if ($this->client->validateParams($settings) !== true) {
      $this->logger->info($this->client->errors);
      return false;
    }
    $fields = $this->client->createFieldsForForm();
    $this->addPostParams($fields); 
    
    return $this->client->getPayments()->paymentUrl;
  }

  /**
   * Возвращает ID заказа исходя из REQUEST-параметров соотвествующего типа оплаты
   * Используется только для Online-платежей
   * 
   * @return mixed
   */
  function getTransactionIdFromRequest(\RS\Http\Request $request) {
    if($request->request('transaction', TYPE_INTEGER, false)){
      return $request->request('transaction', TYPE_INTEGER, false);
    }
    return $request->request('id_order', TYPE_INTEGER, false);
  }

  function onResult(\Shop\Model\Orm\Transaction $transaction, \RS\Http\Request $request) {
    $params = $this->getOption();
    $settings = array(
      'merchantId' => $params['MERCHANT_ID'],
      'signatureMethod' => $params['signatureMethod'],
      'signature' => $params['SIGNATURE'],
      'currencyId' => $params['CURRENCY_ID'],
      'currencyDefault' => $params['currency_default'],
      'orderStatusSuccess' => '',
      'orderStatusWaiting' => '',
    );
    
    $settings['orderPaymentId'] = $_POST['WMI_ORDER_ID'];
    $settings['orderState'] = mb_strtolower($_POST['WMI_ORDER_STATE']);
    $settings['orderId'] = str_replace('_' . $_SERVER['HTTP_HOST'], '', $_POST['WMI_PAYMENT_NO']);
    $settings['paymentType'] = $_POST['WMI_PAYMENT_TYPE'];
    $settings['summ'] = $_POST['WMI_PAYMENT_AMOUNT'];
    if ($this->client->resultValidation($settings, $_POST) == true) {
      $result = $this->client->getResult();
      
      //getting detail of order
      $order = $transaction->getOrder();
      if (empty($order)) {
        $error = sprintf(w1ErrorResultOrder, $result->orderId, $result->orderState, $result->orderPaymentId);
        $this->logger->info($error);
        ob_start();
        echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION='.$error;
        die();
      }
      //checking on the order amount
      if (number_format($transaction->cost, 2, '.', '') != $result->summ) {
        $error = sprintf(w1ErrorResultOrderSumm, $result->orderId, $result->orderState, $result->orderPaymentId);
        $this->logger->info($error);
        ob_start();
        echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION='.$error;
        die();
      }
      if ($result->orderState == 'accepted') {
        $transaction['status'] = \Shop\Model\Orm\Transaction::STATUS_SUCCESS;
        $transaction->update();
        // Если это транзакция оплаты заказа  
        if ($transaction->order_id) {
          if ($transaction->getPayment()->success_status) {
            // Выставляем статус который указан в настройках типа оплаты
            $order->status = $transaction->getPayment()->success_status;
          }
          $order->is_payed = 1; // Ставим пометку "Оплачен"  
          $order->update();

          $notice = new \Shop\Model\Notice\OrderPayed;
          $notice->init($order);
          \Alerts\Model\Manager::send($notice);

          $text = sprintf(w1OrderResultSuccess, $result->orderId, $result->orderState, $result->orderPaymentId);
          $this->logger->info($text);
          ob_start();
          echo 'WMI_RESULT=OK';
          die();
        }
      }
    }
    
    $logger->info('Error');
    ob_start();
    echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION=Error';
    die();
  }

  /**
   * Вызывается при переходе на страницу успеха, после совершения платежа 
   * 
   * @return void 
   */
  function onSuccess(\Shop\Model\Orm\Transaction $transaction, \RS\Http\Request $request) {
    $params = $this->getOption();
    //getting detail of order
    $order = $transaction->getOrder();
    if (empty($order)) {
      $this->logger->info(w1ErrorResultOrderOnlyText);
      throw new \Exception(w1ErrorResultOrderOnlyText);
    }
    
    $router = \RS\Router\Manager::obj();
    $url = \RS\Http\Request::commonInstance();
    
    if ($order->is_payed == 0) {
      header('Location: '.$router->getUrl('w1-front-onlinepayment').'waiting/?id_order='.$url->get('id_order', TYPE_INTEGER));
      exit;
    }
    $text = sprintf(w1OrderResultSuccessOnlyText, $url->get('id_order', TYPE_INTEGER));
    $this->logger->info($text);
  }
  
  /**
    * Вызывается при открытии страницы неуспешного проведения платежа 
    * Используется только для Online-платежей
    * 
    * @param \Shop\Model\Orm\Transaction $transaction
    * @param \RS\Http\Request $request
    * @return void 
    */
    function onFail(\Shop\Model\Orm\Transaction $transaction, \RS\Http\Request $request){
      $transaction['status'] = $transaction::STATUS_FAIL;
      $transaction->update();
    }

}
